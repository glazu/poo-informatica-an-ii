#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstring>

using namespace std;

struct Curs {
    int hStart, mStart, hEnd, mEnd;
    char name[50];
};

struct Student {
    int gr;
    double avg;
    char name[50];
    double grades[];
};

int cmpOrar(const void *p, const void *q);
int cmpStudent(const void *p, const void *q);

int main()
{
    //Ex. 1
    cout << "Ex. 1" << endl;
    int n, *v, pos = 0, neg = 0, *p, *ng, counterPos = 0, counterNeg = 0;
    do {
        cout << "n: ";
        cin >> n;
    } while (n < 2);
    v = new int[n];

    for (int i = 0; i < n; i++) {
        cout << "v[" << i << "]: ";
        cin >> v[i];
        if (v[i] < 0) neg++;
        if (v[i] >= 0) pos++;
    }
    p = new int[pos];
    ng = new int[neg];

    for (int i = 0; i < n; i++) {
        if (v[i] < 0) {
            ng[counterNeg] = v[i];
            counterNeg++;
        } else {
            p[counterPos] = v[i];
            counterPos++;
        }
    }

    if (counterPos > 0) {
        cout << "Numere pozitive: " << p[0];
        for (int i = 1; i < counterPos; i++) {
            cout << ", " << p[i];
        }
    }
    if (counterNeg) {
        cout << "\nNumere negative: " << ng[0];
        for (int i = 1; i < counterNeg; i++) {
            cout << ", " << ng[i];
        }
    }
    cout << endl;

    delete []v;
    delete []p;
    delete []ng;

    //Ex. 2
    cout << "Ex. 2" << endl;
    do {
        cout << "n: ";
        cin >> n;
    } while (n < 0 || n > 9999);
    int **arr = new int*[n];

    for (int i = 0; i < n; i++) {
        arr[i] = new int[i + 1];
        for (int j = 0; j < i + 1; j++) {
            arr[i][j] = j + 1;
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }

    //Ex. 3
    cout << "Ex. 3" << endl;
    ifstream f;
    f.open("../cursuri.txt");
    char c;
    string line;
    for (n = 0; getline(f, line); ++n);
    f.clear();
    f.seekg(0, ios::beg);
    Curs *tc = new Curs[n];

    for (int i = 0; i < n; i++) {
        f >> tc[i].hStart >> c >> tc[i].mStart >> c >> tc[i].hEnd >> c >> tc[i].mEnd;
        f.get(tc[i].name, 50);
    }

    qsort(tc, n, sizeof(Curs), cmpOrar);

    for (int i = 0; i < n; i++) {
        cout << tc[i].hStart << ":" << tc[i].mStart << " - " << tc[i].hEnd << ":" << tc[i].mEnd << " " << tc[i].name << endl;
    }

    //Ex. 4
    cout << "Ex. 4" << endl;
    cout << "Introduceti numarul de studenti." << endl;
    cin >> n;
    Student t[n];
    int nrNote;
    double sumNote;
    for (int i = 0; i < n; i++) {
        sumNote = 0;
        cout << "Nume: ";
        cin >> t[i].name;
        cout << "Grupa: ";
        cin >> t[i].gr;
        cout << "Numar de note: ";
        cin >> nrNote;
        for (int j = 0; j < nrNote; j++) {
            cout << "Nota " << j + 1 << ": ";
            cin >> t[i].grades[j];
            sumNote += t[i].grades[j];
        }
        t[i].avg = sumNote/nrNote;
    }

    qsort(t, n, sizeof(Student), cmpStudent);

    for (int i = 0; i < n; i++) {
        cout << "Nume: " << t[i].name << endl;
        cout << "\tGrupa: " << t[i].gr << endl;
        cout << "\tMedia: " << t[i].avg << endl;
    }

    return 0;
}

int cmpStudent(const void *p, const void *q) {
    const Student *a = (Student*) p;
    const Student *b = (Student*) q;

    double avgCmp = b->avg - a->avg;
    int nameCmp = strcmp(a->name, b->name);

    return (avgCmp == 0) ? nameCmp : (int) avgCmp;
}

int cmpOrar(const void *p, const void *q) {
    const Curs *a = (Curs*) p;
    const Curs *b = (Curs*) q;

    int aStart = a->hStart * 100 + a->mStart;
    int bStart = b->hStart * 100 + b->mStart;

    return aStart - bStart;
}

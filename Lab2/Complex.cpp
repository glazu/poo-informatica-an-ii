#include <iostream>
#include "Complex.h"

using namespace std;

void Complex::init(double re, double im) {
    this->re = re;
    this->im = im;
}

void Complex::afisare() {
    cout << "Numarul complex este: " << re << " + " << im << "i" << endl;
}

void Complex::setRe(double re) {
    this->re = re;
}

void Complex::setIm(double im) {
    this->im = im;
}

Complex Complex::adunare(Complex obj) {
    Complex sum;
    sum.re = this->re + obj.re;
    sum.im = this->im + obj.im;

    return sum;
}
class Complex {
    double re,im;
    public:
        void init (double re, double im);
        void afisare();
        void setRe(double re);
        void setIm(double im);
        Complex adunare(Complex obj);
        //Complex conjugare();
}; // API: interfata unei clase
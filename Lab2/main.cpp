#include <iostream>
#include <math.h>
#include "Complex.h"

using namespace std;

class Punct {
    double x,y;
public:
    void init(double x, double y) {
        this->x = x;
        this->y = y;
    }

    void afisare() {
        cout << x << " " << y << endl;
    }

    void setX(double val) {
        x = val;
    }

    void setY(double val) {
        y = val;
    }

    double distanta(Punct obj) {
        return sqrt(pow(this->x - obj.x, 2) + pow(this->y - obj.y, 2));
    }
};

int main() {
    //================ Ex1
    Punct a,b;
    int x,y;

    cout << "a: x = ";
    cin >> x;
    cout << "y = ";
    cin >> y;

    a.init(x, y);

    cout << "b: x = ";
    cin >> x;
    cout << "y = ";
    cin >> y;

    b.init(x,y);

    a.afisare();
    b.afisare();

    cout << a.distanta(b) << endl;
    //================ Ex2
    cout << "Complex: "<< endl;
    Complex z1, z2, z3;
    z1.init(1, 2);
    z2.init(3,4);

    z3 = z1.adunare(z2);
    z3.afisare();

    z3.setRe(11.5);
    z3.setIm(3.14);
    z3 = z3.adunare(z1);
    z3.afisare();

    return 0;
};
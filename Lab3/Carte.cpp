#include <iostream>
#include "Carte.h"

Carte::Carte() {
    this->titlu = NULL;
    this->an_aparitie = -1;
    this->editura = NULL;
    this->autor = NULL;
}

Carte::Carte(char *titlu, int an) {
    this->titlu = titlu;
    this->an_aparitie = an;
    this->editura = NULL;
    this->autor = NULL;
}

Carte::Carte(char *titlu, char *editura) {
    this->titlu = titlu;
    this->editura = editura;
    this->an_aparitie = -1;
    this->autor = NULL;
}

Carte::Carte(char *titlu, char* autor, char *editura) {
    this->titlu = titlu;
    this->autor = autor;
    this->editura = editura;
    this->an_aparitie = -1;
}

void Carte::afisare() {
    if (this->titlu != NULL)
        std::cout << "Titlu: " << this->titlu << std::endl;
    if (this->autor != NULL)
        std::cout << "\tAutor: " << this->autor << std::endl;
    if (this->an_aparitie != -1)
        std::cout << "\tAn aparitie: " << this->an_aparitie << std::endl;
    if (this->editura != NULL)
        std::cout << "\tEditura: " << this->editura << std::endl;
}

char* Carte::getTitlu() {
    return this->titlu;
}

int Carte::getAnAparitie() {
    return this->an_aparitie;
}

char* Carte::getEditura() {
    return this->editura;
}

char* Carte::getAutor() {
    return this->autor;
}

void Carte::setTitlu(char *T) {
    this->titlu = T;
}

void Carte::setAnAparitie(int A) {
    this->an_aparitie = A;
}

void Carte::setEditura(char *Ed) {
    this->editura = Ed;
}

void Carte::setAutor(char *Au) {
    this->autor = Au;
}

Carte::~Carte() {}
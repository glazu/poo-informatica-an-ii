#ifndef LAB3_CARTE_H
#define LAB3_CARTE_H

class Carte {
    char* titlu;
    int an_aparitie;
    char* editura;
    char* autor;
public:
    Carte();
    Carte(char* titlu, char* autor, char* editura);
    Carte(char* titlu, char* editura);
    Carte(char* titlu, int an);
    ~Carte();
    void afisare ();
    char* getTitlu();
    int getAnAparitie();
    char *getEditura();
    char *getAutor();
    void setTitlu(char *T);
    void setAnAparitie(int A);
    void setEditura(char *Ed);
    void setAutor(char *Au);
};

#endif //LAB3_CARTE_H

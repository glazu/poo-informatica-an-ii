#include "Date.h"
#include <ctime>

Date::Date(int day, int month, int year) {
    this->day = day;
    this->month = month;
    this->year = year;
}

Date::Date(int day, int month) {
    time_t t = time(0);
    struct tm * now = localtime( & t );

    this->day = day;
    this->month = month;
    this->year = now->tm_year + 1900;
}

Date::Date(int day) {
    time_t t = time(0);
    struct tm * now = localtime( & t );

    this->day = day;
    this->month = now->tm_mon + 1;
    this->year = now->tm_year + 1900;
}

Date::Date() {
    time_t t = time(0);
    struct tm * now = localtime( & t );

    this->day = now->tm_mday;
    this->month = now->tm_mon + 1;
    this->year = now->tm_year + 1900;
}

std::string Date::getDate() {
    // c++11 este necesar pentru to_string.
    return std::to_string(this->day) + "/" + std::to_string(this->month) + "/" + std::to_string(this->year);
}

int Date::getYear() {
    return this->year;
}

int Date::getMonth() {
    return this->month;
}

int Date::getDay() {
    return this->day;
}

void Date::setYear(int year) {
    this->year = year;
}

void Date::setMonth(int month) {
    if (month > 0 || month < 13) this->month = month;
}

void Date::setDay(int day) {
    if (day > 0 || day < 32) this->day = day;
}

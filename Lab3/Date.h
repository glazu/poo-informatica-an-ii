#include <iostream>

#ifndef LAB3_DATE_H
#define LAB3_DATE_H

class Date {
    int year;
    int month;
    int day;
public:
    Date(int day, int month, int year);
    Date(int day, int month);
    Date(int day);
    Date();
    std::string getDate();
    int getYear();
    int getMonth();
    int getDay();
    void setYear(int year);
    void setMonth(int month);
    void setDay(int day);
};

#endif //LAB3_DATE_H

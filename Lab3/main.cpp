#include <iostream>
#include <cstring>
#include "Carte.h"
#include "Date.h"

int main() {
    //Ex. 1
    Date d1(15,3,1999);
    Date d2(20,4);
    Date d3(18);
    Date d4;

    std::cout << d1.getDate() << std::endl;
    std::cout << d2.getDate() << std::endl;
    std::cout << d3.getDate() << std::endl;
    std::cout << d4.getDate() << std::endl;

    d1.setYear(1969);
    d3.setDay(31);
    d2.setMonth(1);
    d4.setYear(2038);
    d4.setMonth(1);
    d4.setDay(19);

    std::cout << d1.getDate() << std::endl;
    std::cout << d2.getDate() << std::endl;
    std::cout << d3.getDate() << std::endl;
    std::cout << d4.getDate() << std::endl;

    //Ex. 2
    int n;
    std::cout << "Numarul de carti: ";
    std::cin >> n;
    Carte shelf[n];

    for (int i = 0; i < n; i++) {
        char *T, *Au, *Ed;
        char buff[50];
        int A;
        std::cout << "Titlu: ";
        std::cin >> buff;
        T = new char[strlen(buff)];
        strcpy(T, buff);
        shelf[i].setTitlu(T);
        std::cout << "An aparitie: ";
        std::cin >> A;
        shelf[i].setAnAparitie(A);
        std::cout << "Autor: ";
        std::cin >> buff;
        Au = new char[strlen(buff)];
        strcpy(Au, buff);
        shelf[i].setAutor(Au);
        std::cout << "Editura: ";
        std::cin >> buff;
        Ed = new char[strlen(buff)];
        strcpy(Ed, buff);
        shelf[i].setEditura(Ed);
    }

    for (int i = 0; i < n; i++) {
        shelf[i].afisare();
    }

    return 0;
}
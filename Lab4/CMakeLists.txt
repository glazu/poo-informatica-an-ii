cmake_minimum_required(VERSION 3.9)
project(Lab4)

set(CMAKE_CXX_STANDARD 11)

add_executable(Lab4 main.cpp Stiva.cpp Stiva.h Produs.cpp Produs.h)
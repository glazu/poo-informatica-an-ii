#include <iostream>
#include <cstring>
#include "Produs.h"

Produs::Produs() {
    this->pret = -1;
    this->cantitate = -1;
}

Produs::Produs(char *denumire, double pret, int cantitate) {
    this->denumire = denumire;
    this->pret = pret;
    this->cantitate = cantitate;
}

Produs::Produs(Produs &ob) {
    //TODO: this
}

void Produs::afisare() {
    std::cout << this->denumire << std::endl;
    std::cout << "\tPret: " << this->pret << std::endl;
    std::cout << "\tCantitate: " << this->cantitate << std::endl;
}

void Produs::setDenumire(char *denumire) {
    this->denumire = new char[strlen(denumire)];
    strcpy(this->denumire, denumire);
}

void Produs::setCantitate(int cantitate) {
    this->cantitate = cantitate;
}

void Produs::setPret(double pret) {
    this->pret = pret;
}

char *Produs::getDenumire() {
    return this->denumire;
}

double Produs::getPret() {
    return this->pret;
}

int Produs::getCantitate() {
    return this->cantitate;
}

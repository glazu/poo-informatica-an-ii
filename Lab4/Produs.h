#ifndef LAB4_PRODUS_H
#define LAB4_PRODUS_H


class Produs {
    char *denumire;
    double pret;
    int cantitate;
public:
    Produs();
    Produs(char *denumire, double pret, int cantitate);
    Produs(Produs &ob);
    void afisare();
    void setDenumire(char *denumire);
    void setCantitate(int cantitate);
    void setPret(double pret);
    char *getDenumire();
    double getPret();
    int getCantitate();
};


#endif //LAB4_PRODUS_H

#include <iostream>
#include "Stiva.h"

Stiva::Stiva() {
    this->HEAD = nullptr;
}

Stiva::Stiva(int val) {
    this->HEAD = nullptr;
    this->push(val);
}

void Stiva::push(int val) {
    List *i = HEAD;
    while(i != nullptr && i->next != nullptr) {
        i = i->next;
    }
    List *q = new List;
    q->data = val;
    if (i == nullptr) {
        HEAD = q;
        q->prev = nullptr;
        q->next = nullptr;
    } else {
        i->next = q;
        q->prev = i;
    }
}

Stiva::Stiva(Stiva &s) {
    List *newHEAD = s.getHEAD();
    if (newHEAD == nullptr) {
        this->HEAD = nullptr;
    } else {
        List *i = newHEAD;
        while(i->next != nullptr) {
            this->push(i->data);
            i = i->next;
        }
        this->push(i->data);
    }
}

int Stiva::pop() {
    List *i = HEAD;
    if (HEAD == nullptr) return -1;

    while(i != nullptr && i->next != nullptr) {
        i = i->next;
    }

    int data = i->data;
    if (i->prev == nullptr) {
        HEAD = nullptr;
    } else {
        i->prev->next = nullptr;
    }
    delete i;
    return data;
}

List *Stiva::getHEAD() {
    return this->HEAD;
}

void Stiva::afisare() {
    List *i = HEAD;
    while(i != nullptr && i->next != nullptr) {
        std::cout << i->data << " ";
        i = i->next;
    }
    std::cout << i->data << " ";
    std::cout << std::endl;
}

Stiva::~Stiva() {
    List *i = HEAD;
    while(i != nullptr && i->next != nullptr) {
        List *nextElem = i->next;
        delete i;
        i = nextElem;
    }
}



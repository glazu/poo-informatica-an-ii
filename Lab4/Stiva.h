#ifndef LAB4_STIVA_H
#define LAB4_STIVA_H

struct List {
    List *next;
    List *prev;
    int data;
};

class Stiva {
    List *HEAD = nullptr;
public:
    Stiva();
    Stiva(int val);
    Stiva(Stiva&);
    void push(int val);
    int pop();
    List *getHEAD();
    void afisare();
    ~Stiva();
};


#endif //LAB4_STIVA_H

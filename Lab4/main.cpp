#include <iostream>
#include "Stiva.h"
#include "Produs.h"
#include <fstream>
#include <string.h>

int main() {
    //Ex. 1
    Stiva s(5);
    s.push(16);
    s.push(22);

    Stiva ss(s);
    ss.push(3);
    ss.push(4);

    s.afisare();
    ss.afisare();



    //Ex. 2
    int n, cantitate;
    double pret;
    char nume[50];
    std::ifstream f;
    f.open("../Produse.txt");
    char c;
    std::string line;
    for (n = 0; getline(f, line); ++n);
    f.clear();
    f.seekg(0, std::ios::beg);
    Produs t[n];


    for (int i = 0; i < n; i++) {
        f >> nume >> pret >> cantitate;
        t[i].setDenumire(nume);
        t[i].setPret(pret);
        t[i].setCantitate(cantitate);
    }


    std::ofstream of;
    of.open("../ValoareStoc.txt");
    for(int i = 0; i < n; i++) {
        of << t[i].getDenumire() << " " << t[i].getCantitate() << "\n";
    }

    return 0;
}
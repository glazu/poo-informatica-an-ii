#include <iostream>
#include "Automobil.h"
#include <string.h>

using namespace std;

int Automobil::nrAuto = 0;
const double Automobil::TVA = 0.24;

Automobil::Automobil() {
    marca = new char[4];
    strcpy(marca, "###");
    capacitate = 0;
    pret = 0.0;
    nrAuto++;
    hasCustomMarca = false;
};

Automobil::Automobil(char *marca, int capaciate, double pret) {
    this->marca = new char(strlen(marca) + 1);
    strcpy (this->marca, marca);
    this->capacitate = capacitate;
    this->pret = pret;
    nrAuto++;
    hasCustomMarca = false;
}

Automobil::Automobil(const Automobil &ob) {
    this->marca = new char[strlen(ob.marca) + 1];
    strcpy(this->marca, ob.marca);
    this->capacitate = ob.capacitate;
    this->pret = ob.pret;
    hasCustomMarca = false;
}

Automobil::~Automobil() {
    //if (marca)
    delete []marca;
}
void Automobil::afisare() {
    if (strcmp(marca, "###")) cout << marca << " ";
    if (capacitate != 0) cout << capacitate << " ";
    if (pret != 0.0) cout << pret << endl;
}

void Automobil::setMarca(char *v) {
    if (marca) delete []marca;
    marca = new char[strlen(v) + 1];
    strcpy(marca, v);
}

void Automobil::setCapacitate(int v) {
    this->capacitate = v;
}

void Automobil::setPret(double v) {
    this->pret = v;
}

char * Automobil::getMarca() {
    return marca;
}

double Automobil::getPret() {
    return pret;
}

int Automobil::nrAutomobil() {
    return nrAuto;
}

int Automobil::getCapacitate() {
    return capacitate;
}

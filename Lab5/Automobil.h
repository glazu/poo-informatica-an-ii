class Automobil {
    char *marca;
    int capacitate;
    double pret;
    static const double TVA;
    public:
        bool hasCustomMarca;
        static int nrAuto;
        Automobil();
        Automobil(char * marca, int capacitate, double pret);
        void afisare();
        void setMarca(char *v);
        void setCapacitate(int v);
        void setPret(double v);
        char * getMarca();
        int getCapacitate();
        double getPret();
        static int nrAutomobil();
        Automobil(const Automobil &ob);
        ~Automobil();
};
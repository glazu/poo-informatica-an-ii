#include <iostream>
#include "Automobil.h"
#include <string.h>
#include <fstream>
#include <stdlib.h>

using namespace std;

int main()
{
    Automobil ta[20];

    ifstream f("../Automobil.txt");
    char linie[500];

    int i = 0;
    while(f.getline(linie, 500)) {
        char *p = strtok(linie, " ");
        ta[i].setMarca(p);
        p = strtok(NULL, " ");
        ta[i].setCapacitate(atoi(p));
        p = strtok(NULL, " ");
        ta[i].setPret(atoi(p));
        i++;
    }

    double maxPret;
    cout << "Afisati automobilele cu un pret mai mic decat: ";
    cin >> maxPret;

    cout << "Setati marca primului automobil: ";
    char aux[50];
    cin >> aux;
    char *v = new char[strlen(aux)];
    strcpy(v, aux);
    ta[0].setMarca(v);
    ta[0].hasCustomMarca = true;

    for (i = 0; i < 20 && ta[i].getPret() <= maxPret; i++) {
        ta[i].afisare();
    }

    std::ofstream of;
    of.open("../MarcaXXX.txt");

    for (i = 0; i < 20 && ta[i].hasCustomMarca; i++) {
        of << ta[i].getMarca() << " " << ta[i].getCapacitate() << " " << ta[i].getPret() << "\n";
    }

    return 0;
}